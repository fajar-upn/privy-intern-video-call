import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import CreateRoom from "./components/CreateRoom"
import Room from "./components/Room"

import CaptureImage from "./components/CaptureImage";
import RecordVideo from "./components/RecordVideo";



function App() {

  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={CreateRoom}></Route>
          <Route path="/room/:roomID" component={Room}></Route>
        </Switch>
      </BrowserRouter>

      {/* <button onClick={CaptureImage}>Take Screenshoot</button> */}
      {/* <RecordVideo /> */}

    </div>
  )
}

export default App
